
import * as React from 'react';
import { Button, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from  '../screen/Home';
import ProfileStack from '../screen/Home'
import DrawerProfile from '../component/DrawerProfile'
import MapSearch from '../screen/MapSearch';

const mainRouter=()=> {
      const Drawer = createDrawerNavigator();
        return (
            <Drawer.Navigator initialRouteName="Home" drawerContent={props=> <DrawerProfile {...props} />} >
               <Drawer.Screen name="Home" component={Home} />
            </Drawer.Navigator>
        );
}
export default mainRouter;


