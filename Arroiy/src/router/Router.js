import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screen/Login';
import Register from '../screen/Register';
import FirstUseApp from '../screen/FirstUseApp';
import StoreDetail from '../screen/StoreDetail';
import Profile from '../screen/Profile';
import AddStore from '../screen/AddStore';
import mainRouter from './mainRouter';
import MapSearch from '../screen/MapSearch';
import AddMap from '../screen/AddMap';
import StoreMap from '../screen/StoreMap';
import ProfileEdit from '../screen/ProfileEdit'; 
import EditStore  from '../screen/EditStore';
import AddImageProfile from '../screen/AddImageProfile';
import MapAll from '../screen/MapAll';
import EditProfileImage from '../screen/EditProfileImage';
import AddImage  from '../screen/AddImage'

const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>         
                <Stack.Screen name="FirstUseApp" component={FirstUseApp}  options = {{ headerShown: false }}/>
                <Stack.Screen name="mainRouter" component={mainRouter} options = {{ headerShown: false }}/>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="StoreDetail" component={StoreDetail} />
                <Stack.Screen name="Profile" component={Profile} />
                <Stack.Screen name="AddStore" component={AddStore} />
                <Stack.Screen name="MapSearch" component={MapSearch} />
                <Stack.Screen name="AddMap" component={AddMap} />
                <Stack.Screen name="StoreMap" component={StoreMap} />
                <Stack.Screen name="ProfileEdit" component={ProfileEdit} />
                <Stack.Screen name="EditStore" component={EditStore} />
                <Stack.Screen name="AddImageProfile" component={AddImageProfile} />
                <Stack.Screen name="MapAll" component={MapAll} />
                <Stack.Screen name="EditProfileImage" component={EditProfileImage} />
                <Stack.Screen name="AddImage" component={AddImage} />

            </Stack.Navigator>
        </NavigationContainer>
    );
}

