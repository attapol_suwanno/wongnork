import React, { useEffect, useState } from 'react';
import { View, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import { Avatar, List, Headline, Subheading, Dialog, IconButton, Colors } from 'react-native-paper';
import axios from 'axios';
import { connect } from 'react-redux';

const Profile = (props) => {
    const [allDataStore, setDataStore] = useState([])
    const [dataUser, setDatauser] = useState([])
    const [isloading, setIsloading] = useState(false)
    const [datalist, setDatalist] = useState([])

    useEffect(() => {
        Profile()
    }, [])

    const Profile = () => {
        axios.get('http://wongnork.unomy.pw/user/profile', {
            headers: {
                Authorization: `Bearer ${props.tokenUser}`
            }
        })
            .then(response => {
                setDatauser(response.data),
                    getAllRestaurent(response.data._id)
                console.log('ppppp', response.data._id)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const getAllRestaurent = (thisId) => {
        axios.get('http://wongnork.unomy.pw/restaurant?', {
            params: {
                limit: 1000
            }
        })
            .then(response => {
                setDataStore(response.data.list)
                setDatalist(response.data.list.filter((value, index) => {
                    if (value.userId === thisId) {
                        return value
                    }
                }))
            })
            .catch(error => {
                console.log('error', JSON.stringify(error))
            })
    }

    const myRendernumber = (item, index) => {
        return (
            <TouchableOpacity onPress={() => props.navigation.navigate('EditStore', { dataStore: item, indexStore: index, idStore: item._id })} >
                <List.Section style={styles.border} >
                    <List.Item
                        left={() => (
                            <Image
                                source={{ uri: (item.image) }}
                                style={styles.image}
                            />
                        )}
                        title={item.name}
                        description={item.description}
                    />
                </List.Section>
            </TouchableOpacity>
        )
    }

    const workingRefresh = () => {
        setIsloading(true)
        setTimeout(() => {
            setIsloading(false)
        }, 2000)
    }

    return (
        <View style={{ flex: 1, }} >
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 1, }}  >
                    <View style={{ flex: 1, flexDirection: 'row', borderRadius: 40, backgroundColor: "#ffa726", }} >
                        <View style={{ flex: 1, flexDirection: 'row', }} >
                            <View style={{ flex: 1, flexDirection: 'row' }} >
                                <Image style={styles.stretch}
                                    source={{ uri: (dataUser.profileImage) }}
                                >
                                </Image>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', }} >
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginRight: '7%', marginTop: '5%' }} >
                                <TouchableOpacity onPress={() => props.navigation.navigate('ProfileEdit', { sentUserdata: dataUser })} >
                                    <Avatar.Icon size={40} icon="settings" style={{ backgroundColor: Colors.lightGreen200 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row' }} >
                                <Headline>{dataUser.firstName}</Headline>
                            </View>
                            <View style={{ flexDirection: 'row' }} >
                                <Subheading>{dataUser.username}</Subheading>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', }}  >
                        <IconButton
                            icon="plus"
                            size={36}
                            onPress={() => props.navigation.navigate('AddStore', { dataStore: allDataStore, id: dataUser._id })}
                            style={{ backgroundColor: Colors.lightGreen200 }}
                        />
                    </View>
                    <View style={{ flex: 3, flexDirection: 'row', }}  >
                        <Dialog.ScrollArea>
                            <ScrollView >
                                <FlatList
                                    data={datalist}
                                    renderItem={({ item, index }) => myRendernumber(item, index)}
                                    style={{ hight: 100, marginTop: '2%' }}
                                    keyExtractor={(item, index) => index + ''}
                                    refreshing={isloading}
                                    onRefresh={workingRefresh}>
                                </FlatList>
                            </ScrollView>
                        </Dialog.ScrollArea>
                    </View>
                </View>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    stretch: {
        width: '55%',
        height: '60%',
        borderRadius: 100,
        marginTop: '15%',
        justifyContent: 'center',
        marginLeft: '20%'
    },
    image: {
        height: 40,
        width: 40,
        margin: 8,
    },
    row: {
        flexDirection: 'row',
    },
    column: {
        flexDirection: 'column',
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}

export default connect(mapStateToProps, null)(Profile)