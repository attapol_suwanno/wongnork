import React, { useEffect, useState, useRef } from 'react';
import { View, Text, StatusBar, SafeAreaView, StyleSheet,FlatList, TouchableOpacity, Animated, Dimensions,Image  } from 'react-native';
import {  Card, Title, IconButton, Avatar, Colors } from 'react-native-paper';
import axios from 'axios';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { connect } from 'react-redux';
import { logOut } from '../action/LoginAction';
import { useFocusEffect } from '@react-navigation/native';
import { addUserProfile } from '../action/UserAction'
const { width: screenWidth } = Dimensions.get('window')

const Home = (props) => {
    const [dataRestaurent, setDataRestaurent] = useState([]);
    const [dataUser, setDatauser] = useState([])
    const [isloading, SetIsloading] = useState(false)
    const [fadeValue] = useState(new Animated.Value(0))
    const [storeNearMe, setStoreNeearMe] = useState([])
    const [offsetValue, setOffsetValue] = useState(0)
    const carouselRef = useRef(null)
    const [callReached, setCallReached] = useState()
    const { firstlatitude, firstlongitude } = props

    useEffect(() => {
        fetchDataRestaurant(),
            Animated.timing(fadeValue, {
                toValue: 1,
                duration: 4000
            }).start();
        getStoreNearMe();
        Profile();

    }, [])

    useFocusEffect(
        React.useCallback(() => {
            fetchDataRestaurant()
            getStoreNearMe()
        }, [])
    );

    const fetchDataRestaurant = (offsetValue = 0) => {
        axios.get('http://wongnork.unomy.pw/restaurant?offset=' + offsetValue)
            .then(response => {
                console.log('off', offsetValue)
                setDataRestaurent([...dataRestaurent, ...response.data.list])
                SetIsloading(false)
            })
            .catch(error => {
                console.log('eror', error.response)
            })
    }

    const getStoreNearMe = () => {
        axios.get('http://wongnork.unomy.pw/restaurant?', {
            params: {
                latitude: firstlatitude, longitude: firstlongitude, maxDistanceMeters: 1000
            }
        })
            .then(response => {
                setStoreNeearMe(response.data.list)
            })
            .catch(error => {
                console.log('error', JSON.stringify(error))
            })
    }

    const Profile = () => {
        axios.get('http://wongnork.unomy.pw/user/profile', {
            headers: {
                Authorization: `Bearer ${props.tokenUser}`
            }
        })
            .then(response => {
                props.addUserProfile(response.data)
                addUserProfile(response.data)
                setDatauser(response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const workingRefresh = () => {
        SetIsloading(true)
        setTimeout(() => {
            SetIsloading(false)
        }, 1)
    }

    const renderButton = () => {
        if (props.tokenUser !== null) {
            return <Animated.View style={{ opacity: fadeValue, }} >
                <TouchableOpacity style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={() => props.navigation.navigate('Profile', { sentdataStore: dataRestaurent })}  >
                    <Avatar.Image size={45}
                        source={{ uri: (dataUser.profileImage) }}
                    />
                </TouchableOpacity>
            </Animated.View>
        }
    }

    const goForward = () => {
        carouselRef.current.snapToNext()
    }

    const _renderItem = ({ item, index }, parallaxProps) => {
        return (
            <TouchableOpacity onPress={() => props.navigation.navigate('StoreDetail', { dataStore: item, indexStore: index })} >
                <View style={styles.item}>
                    <ParallaxImage
                        source={{ uri: item.image }}
                        containerStyle={styles.imageContainer}
                        style={styles.image}
                        parallaxFactor={0.1}
                        {...parallaxProps}
                    />
                    <Text style={styles.title} numberOfLines={3}>
                        {item.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }


    const onUseloadMore = () => {
        setOffsetValue(parseInt(offsetValue) + parseInt(10))
        fetchDataRestaurant(parseInt(offsetValue) + parseInt(10))
        SetIsloading(true)
    }

    return (
        <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
            <FlatList
                data={dataRestaurent}
                style={{ height: '100%', width: '100%' }}
                keyExtractor={(item, index) => index + ''}
                refreshing={isloading}
                onRefresh={workingRefresh}
                onMomentumScrollBegin={() => setCallReached(true)}
                onEndReached={() => {
                    if (callReached) {
                        onUseloadMore()
                    }
                    setCallReached(false)
                }}
                onEndReachedThreshold={0.01}
                ListHeaderComponent={
                    <>
                        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}  >
                            <View style={{ flex: 1, flexDirection: 'row', }}  >
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginTop: '7%', marginLeft: '5%', marginRight: '5%' }}  >
                                    {renderButton()}
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginTop: '7%', marginLeft: '5%', marginRight: '5%' }}  >
                                    <IconButton
                                        icon="map"
                                        size={30}
                                        onPress={() => props.navigation.navigate('MapAll', { sentDataRestaurent: dataRestaurent })}
                                        style={{ backgroundColor: Colors.yellow700 }}
                                    />
                                </View>
                            </View>
                            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%' }}  >
                                <View style={styles.container}>
                                    <TouchableOpacity onPress={goForward}>
                                        <Title>Near Me</Title>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', elevation: 45,marginTop: '3%' }}  >
                                <Carousel
                                    ref={carouselRef}
                                    sliderWidth={screenWidth}
                                    sliderHeight={screenWidth}
                                    itemWidth={screenWidth - 100}
                                    data={storeNearMe}
                                    renderItem={_renderItem}
                                    hasParallaxImages={true}
                                />
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: '3%' }}  >
                                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', padding: '5%' }}  >
                                    <Title>All restaurant</Title>
                                </View>
                            </View>
                        </View>
                    </>
                }
                renderItem={({ item, index }) => (
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity onPress={() => props.navigation.navigate('StoreDetail', { dataStore: item, indexStore: index })}>
                            <Card style={styles.card}>
                                <Card.Cover
                                    source={{ uri: (item.image) }}
                                />
                                <Card.Title
                                    title={item.name}
                                    left={props => <Avatar.Icon {...props} icon="food" style={{ backgroundColor: Colors.yellow700 }} />}
                                    subtitle={item.description}
                                />
                            </Card>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    container: {
        flex: 1,
    },
    content: {
        padding: 4,
    },
    card: {
        margin: 10,
        elevation: 25,
        padding: 14,
    },
    item: {
        width: screenWidth - 100,
        height: screenWidth - 100,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 9,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
    },

});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
        firstlatitude: state.locationFirst.latitude,
        firstlongitude: state.locationFirst.longitude
    }
}

const mapDispatchToProps = {
    logOut,
    addUserProfile
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)



