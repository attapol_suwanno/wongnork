import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity, } from 'react-native';
import { Button, TextInput, Avatar, Headline} from 'react-native-paper';
import { connect } from 'react-redux';
import axios from 'axios';
import { useFocusEffect } from '@react-navigation/native';

const ProfileEdit = (props) => {
    const [imagePro, setImagePro] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [dataUser, setDatauser] = useState([])


    useFocusEffect(
        React.useCallback(() => {
            setDatauser((props.route.params.sentUserdata))
            setFirstName((props.route.params.sentUserdata.firstName))
            setLastName((props.route.params.sentUserdata.lastName))
            setImagePro((props.route.params.sentUserdata.profileImage))
          
        }, []) 
    );

    useEffect(() => {
        setDatauser((props.route.params.sentUserdata))
        setFirstName((props.route.params.sentUserdata.firstName))
        setLastName((props.route.params.sentUserdata.lastName))
        setImagePro((props.route.params.sentUserdata.profileImage))

    }, [])

    const editProfile = () => {
        axios.put('http://wongnork.unomy.pw/user/profile', { firstName: firstName, lastName: lastName }, {
            headers: {
                Authorization: `Bearer ${props.tokenUser}`
            }
        })
            .then(response => {
                alert('Edit Username & Firstname Successful And You Want EDit Image Onpress in Image Profile')
            })
            .catch(error => {
                console.log('errror', error)
            })
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}>
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'row', borderRadius: 40, backgroundColor: "#ffa726", marginLeft: '10%', marginRight: '10%', marginTop: '10%' }} >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: '2%' }} >
                            <TouchableOpacity onPress={() => props.navigation.navigate('EditProfileImage', { sentDataProfile: dataUser })}  >
                                <Avatar.Image size={80}
                                    source={{ uri: (imagePro) }}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <Headline>{firstName}</Headline>
                        </View>
                    </View>
                    <View style={{ flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderRadius: 40, backgroundColor: "#ffa726", margin: '10%' }} >
                        <TextInput
                            mode='outlined'
                            style={{ width: "70%" }}
                            label='Username'
                            value={firstName}
                            onChangeText={text => setFirstName(text)}
                        />
                        <TextInput
                            mode='outlined'
                            style={{ width: "70%" }}
                            label='Firstname'
                            value={lastName}
                            onChangeText={text => setLastName(text)}
                        />
                        <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" marginTop='5%' onPress={editProfile} >
                            <Text> Edit Profile</Text>
                        </Button>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}
export default connect(mapStateToProps, null)(ProfileEdit)





