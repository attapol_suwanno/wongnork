import React, { useState, useEffect, useRef } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image } from 'react-native';

const StoreMap = (props) => {
    const [imagePro, setImagePro] = useState('')
    const MapViewRef = useRef()

    useEffect(() => {
        setAllDataStore()
        MapViewRef.current.animateCamera({
            center: {
                latitude: props.route.params.DataStore.geoLocation.coordinates[1],
                longitude: props.route.params.DataStore.geoLocation.coordinates[0],
            }
        })
    }, [])

    const setAllDataStore = () => {
        setImagePro((props.route.params.sentpicture))
    }
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'wihte' }}
                source={{ uri: 'https://i.pinimg.com/originals/74/44/03/744403ceee06ccfe326f279a83a61934.jpg' }}
            >
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 3, }}  >
                        <View style={styles.container}>
                            <MapView
                                ref={MapViewRef}
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.map}
                                region={{
                                    latitude:props.route.params.DataStore.geoLocation.coordinates[1],
                                    longitude:props.route.params.DataStore.geoLocation.coordinates[0],
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                }}
                            >
                                <Marker 
                                    coordinate={{
                                        latitude:props.route.params.DataStore.geoLocation.coordinates[1],
                                        longitude:props.route.params.DataStore.geoLocation.coordinates[0],
                                    }}
                                    centerOffset={{ x: -18, y: -60 }}
                                    anchor={{ x: 0.69, y: 1 }}
                                >
                                     <Image  source={{ uri:(imagePro) }} style={{height: 35, width:35, borderRadius: 25, }} />
                                </Marker>
                            </MapView>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
});

export default StoreMap