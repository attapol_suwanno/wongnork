import React, { useState, useCallback, useRef } from 'react';
import axios from 'axios'
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image } from 'react-native';
import { Button, TextInput,HelperText} from 'react-native-paper';
import { connect } from 'react-redux';


const Register = (props) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setlastName] = useState('')
    const [checkAvailable, setCheckAvailable] = useState(null)

    const usernameInput = useRef()
    const passwordInput = useRef()
    const firstNameInput = useRef()
    const lastNameInput = useRef()

    const checkUsername = (text) => {
       
        axios.get(`http://wongnork.unomy.pw/auth/checkAvailability?username=${text}`)
            .then(response => {
                if (response) {
                    setCheckAvailable(response.data.isAvailable) 
                    setUsername(text)
                }
                else {
                    Alert.alert(
                        'กรุณาตรวจสอบข้อมูล',
                        'Username ที่คุณระบุ มีการใช้งานอยู่แล้ว โปรดใช้ Username อื่น'
                    )
                }
            })
            .catch(error=>{
                console.log('error',error.response)
            })
    }

    const addRegister = useCallback(() => {
        axios.post('http://wongnork.unomy.pw/auth/register', { username: username, password: password, firstName: firstName, lastName: lastName })
            .then(response => {
                props.navigation.navigate('Login')
                    .finally(() => {
                        setIsLoadingRegister(false)
                    })
            }
            )
    }, [username, password, firstName, lastName])

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'wihte' }}
                source={{ uri: 'https://i.pinimg.com/originals/74/44/03/744403ceee06ccfe326f279a83a61934.jpg' }}
            >
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', padding: '10%' }}  >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                            <TextInput
                                ref={usernameInput}
                                mode='outlined'
                                style={{ width: "80%" }}
                                label='Username'
                                value={username}
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={text => checkUsername(text)}
                                onSubmitEditing={() => passwordInput.current.focus()}
                            />
                        </View>
                        <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', }}  >
                            <HelperText
                                type="error"
                                visible={checkAvailable ==false}
                            > 
                            Username is invalid!
                            </HelperText>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >

                            <TextInput
                                ref={passwordInput}
                                mode='outlined'
                                style={{ width: "80%" }}
                                label='Password'
                                value={password}

                                secureTextEntry
                                textContentType="password"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={text => setPassword(text)}
                                onSubmitEditing={() => firstNameInput.current.focus()}

                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                            <TextInput
                                ref={firstNameInput}
                                mode='outlined'
                                style={{ width: "80%" }}
                                label='FirstName'
                                value={firstName}
                                onChangeText={text => setFirstName(text)}
                                onSubmitEditing={() => lastNameInput.current.focus()}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                            <TextInput
                                ref={lastNameInput}
                                mode='outlined'
                                style={{ width: "80%" }}
                                label='Lastname'
                                value={lastName}
                                onChangeText={text => setlastName(text)}
                                onSubmitEditing={() => passwordInput.current.focus()}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                            <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={addRegister} >
                                <Text> Register</Text>
                            </Button>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },

});


const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}

export default connect(mapStateToProps, null)(Register)

