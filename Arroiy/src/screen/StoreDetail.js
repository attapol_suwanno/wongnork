import React, { useState, useEffect } from 'react';
import { View, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import {  Avatar, Paragraph, Title,  Colors, } from 'react-native-paper';


const StoreDetail = (props) => {
    const [allDataStore, setDataStore] = useState({})
    const [imagePro, setImagePro] = useState('')
    const [open, setOpen] = useState(false)

    useEffect(() => {
        setAllDataStore()
    }, [])

    const setAllDataStore = () => {
        setDataStore((props.route.params.dataStore)),
            setImagePro((props.route.params.dataStore.image))
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: Colors.yellow700}}
            >
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, flexDirection: 'column' }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 2, flexDirection: 'row' }}  >
                        <ImageBackground
                            source={{ uri: (imagePro) }}
                            style={{ width: '100%', height: '100%' }}>
                            <View style={{ position: 'absolute', top: 0, left: 0, right: '5%', bottom: '5%', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <TouchableOpacity
                                    onPress={() => props.navigation.navigate('StoreMap', { DataStore: props.route.params.dataStore, indexUser: props.route.params.indexStore, sentpicture: props.route.params.dataStore.image })}
                                >
                                    <Avatar.Icon size={50} icon="map" style={{ backgroundColor: Colors.lightGreen200 }} />
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', borderRadius: 40, backgroundColor: "white", }} >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}  >
                            <Title>{allDataStore.name}</Title>
                        </View>
                        <View style={{ flex: 11, flexDirection: 'column', justifyContent: 'center', margin: '10%' }}  >
                            <Paragraph>
                                {allDataStore.description}
                            </Paragraph>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

export default StoreDetail