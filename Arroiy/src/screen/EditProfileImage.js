import React, { useEffect, useState } from 'react';
import { View, Text, StatusBar, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Avatar, Title } from 'react-native-paper';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';

const EditProfileImage = (props) => {
    const [dataUser, setDatauser] = useState([])

    useEffect(() => {
        setDatauser(props.route.params.sentDataProfile)
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            setDatauser(props.route.params.sentDataProfile)
        }, [])
    );

    const changeImage = () => {
        ImagePicker.showImagePicker({}, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                const formData = new FormData()
                formData.append('image', {
                    uri: image.uri,
                    name: image.fileName,
                    type: image.type,
                })
                axios.put(`http://wongnork.unomy.pw/user/profile/image`, formData, {
                    headers: {
                        Authorization: `Bearer ${props.tokenUser}`
                    }
                })
                    .then(response => {
                        console.log('res', response.data)
                        alert('Edit Image Successful')
                            (props.navigation.navigate('ProfileEdit'))
                    })
                    .catch(error => {
                        console.log('eror', JSON.stringify(error))
                    })
            }
        });
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 1, flexDirection: 'column', borderRadius: 40, backgroundColor: "#ffa726", alignItems: 'center', marginTop: '10%',margin:'5%' }} >
                    <View style={{ flex: 0, flexDirection: 'column', marginTop: '10%' }} >
                        <TouchableOpacity onPress={changeImage} >
                            <Avatar.Image size={200}
                                source={{ uri: (dataUser.profileImage) }}
                            />
                        </TouchableOpacity>
                    </View>
                    <Title>{dataUser.username}</Title>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                    <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={changeImage} >
                        <Text> EditImage</Text>
                    </Button>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },

});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}

export default connect(mapStateToProps, null)(EditProfileImage)
