import React, { useEffect } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, } from 'react-native';
import { Button, Card } from 'react-native-paper';
import { connect } from 'react-redux';

const FirstUseApp = (props) => {
    const { firstlatitude, firstlongitude } = props

    useEffect(() => {
        if (firstlatitude !== null && firstlongitude !== null) {
            props.navigation.navigate('mainRouter')
        }
    }, [])


    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'wihte' }}
                source={{ uri: 'https://i.pinimg.com/originals/74/44/03/744403ceee06ccfe326f279a83a61934.jpg' }}
            >
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                    <View style={{ flex: 1 }} >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '10%', }}  >
                            <Card.Cover source={{ uri: 'https://cliply.co/wp-content/uploads/2019/03/371903340_LOCATION_MARKER_400.gif' }} style={{ width: 200, height: 200, borderTopLeftRadius: 100, borderTopRightRadius: 100, borderRadius: 100, elevation: 50, }} />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', padding: '5%' }}  >
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '5%' }}  >
                                <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={() => props.navigation.navigate('MapSearch')} >
                                    <Text> Get Started</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },

});


const mapStateToProps = state => {
    return {
        firstlatitude: state.locationFirst.latitude,
        firstlongitude: state.locationFirst.longitude
    }
}

export default connect(mapStateToProps, null)(FirstUseApp)

