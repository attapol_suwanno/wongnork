import React, { useState, useEffect, useRef } from 'react';
import MapView, { PROVIDER_GOOGLE ,Marker} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet,  } from 'react-native';
import { Button } from 'react-native-paper';
import Geolocation from 'react-native-geolocation-service'
import { check, request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions'
import {addLocationFirst} from '../action/FisrtUseAction';
import { connect} from 'react-redux';

const MapSearch = (props) => {
    const [thisLocation, setThisLocation]=useState({
        latitude: 18.796470,
        longitude: 98.953280,
    })

    const options = {
        title: 'Select Avatar',
        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
            skipBackup: true,
            path: 'images',
        }
    };

    const MapViewRef = useRef()
    useEffect(() => {
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
            if (result === RESULTS.GRANTED) {
                callLocation((position) => {
                   setThisLocation(position.coords)
                })
                return
            }
            if (result === RESULTS.DENIED) {
                request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
                    if (result === RESULTS.GRANTED) {
                        callLocation((position) => {
                            setThisLocation(position.coords)})
                    }
                    else if (result === RESULTS.DENIED) {
                        alert('Please Allow to Access this device Location')
                    }
                    else if (result === RESULTS.BLOCKED) {
                        alert('You can Allow again to Use Any feature')
                    }
                })
            }
        })
    }, [])

    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)

            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }
    const  changeRegion =(coords)=>{
        setThisLocation( coords)

    }
    const   onClickAdd = () => {
        props.addLocationFirst(thisLocation.latitude,thisLocation.longitude)
        props.navigation.navigate('mainRouter', {screen:'Home',params:{sentlatitude  :thisLocation.latitude, sentlongitude  :thisLocation.longitude}})
    }


    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'wihte' }}
                source={{ uri: 'https://i.pinimg.com/originals/74/44/03/744403ceee06ccfe326f279a83a61934.jpg' }}
            >
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 3, }}  >
                        <View style={styles.container}>
                            <Button>min</Button>
                            <MapView
                                ref={MapViewRef}
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.map}
                                region={{
                                    latitude: thisLocation.latitude,
                                    longitude: thisLocation.longitude,
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                }}
                            >
                                <Marker draggable
                                    coordinate={thisLocation}
                                    onDragEnd={(e) => changeRegion(e.nativeEvent.coordinate)}
                                />
                            </MapView>
                            <Button style={styles.button} color="#18ffff" icon="map-marker-plus" mode="contained"  onPress={onClickAdd}>
                            <Text> AddMap</Text>
                        </Button>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },

});


const mapDispatchToProps = {
    addLocationFirst
    }
    
export default connect(null, mapDispatchToProps)(MapSearch)
    
    