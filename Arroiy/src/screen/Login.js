import React, { useState, useEffect } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image } from 'react-native';
import { Button, TextInput, } from 'react-native-paper';
import axios from 'axios';
import { connect } from 'react-redux';
import { addTokenId } from '../action/LoginAction';

const Login = (props) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const loginGetToken = () => {
        axios.post('http://wongnork.unomy.pw/auth/login', ({ username, password }))
            .then(response => {
                props.addTokenId(response.data.token)
                Profile(response.data.token)
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.data.messages.includes('incorrect/username')) {
                        alert('Username incorrect ')
                    }
                    if (error.response.data.messages.includes('incorrect/password')) {
                        alert('Password incorrect')
                    }
                }
                else {
                    alert('Internet disconnect')
                }
            })
    }

    const Profile = (token) => {
        axios.get('http://wongnork.unomy.pw/user/profile', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                onPresslogin(response.data)
            })
            .catch(error => {
                console.log('eror', JSON.stringify(error))
            })
    }

    const onPresslogin = (item) => {
        if (item.profileImage === undefined) {
            (props.navigation.navigate('AddImageProfile', { sentDataProfile: item }))
        } else {
            (props.navigation.navigate('Home'))
        }
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'wihte' }}
                source={{ uri: 'https://i.pinimg.com/originals/74/44/03/744403ceee06ccfe326f279a83a61934.jpg' }}
            >
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                    <View style={{ flex: 1 }} >
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', marginTop: '10%', }}  >
                            <Image
                                style={{ width: 170, height: 170 }}
                                source={{
                                    uri: 'https://www.pngrepo.com/download/191950/food-cart-circus.png',
                                }}></Image>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', padding: '5%' }}  >
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                                <TextInput
                                    mode='outlined'
                                    style={{ width: "80%" }}
                                    label='Username'
                                    value={username}
                                    onChangeText={text => setUsername(text)}
                                />
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                                <TextInput
                                    mode='outlined'
                                    style={{ width: "80%" }}
                                    label='Password'
                                    secureTextEntry
                                    value={password}
                                    onChangeText={text => setPassword(text)}
                                />
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                                <Button style={styles.button} color="#64ffda" icon="logout-variant" mode="contained" onPress={loginGetToken} >
                                    <Text> Login</Text>
                                </Button>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                                <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={() => props.navigation.navigate('Register')} >
                                    <Text> Register</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },

});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}

const mapDispatchToProps = {
    addTokenId
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
