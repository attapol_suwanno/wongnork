import React, { useState, useEffect, useRef } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, Text, StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, Dimensions } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { Colors } from 'react-native-paper';
import { check, request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions'
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

const MapAll = (props) => {

    const [thisLocation, setThisLocation] = useState({
        latitude: 18.796470,
        longitude: 98.953280,
    })
    const [allLocation, setAllLocation] = useState([])
    const MapViewRef = useRef()

    useEffect(() => {
        setAllLocation(props.route.params.sentDataRestaurent)
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
            if (result === RESULTS.GRANTED) {
                callLocation((position) => {
                    setThisLocation(position.coords)
                })
                return
            }
            if (result === RESULTS.DENIED) {
                request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
                    if (result === RESULTS.GRANTED) {
                        callLocation((position) => {
                            setThisLocation(position.coords)})
                    }
                    else if (result === RESULTS.DENIED) {
                        alert('Please Allow to Access this device Location')
                    }
                    else if (result === RESULTS.BLOCKED) {
                        alert('You can Allow again to Use Any feature')
                    }
                })
            }
        })
    }, [])

    const callLocation = (callback) => {
        Geolocation.getCurrentPosition(
            (position) => {
                callback(position)
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        )
    }

    const renderCarouselItem = ({ item }) =>
        <View style={styles.cardContainer}>
            <Text style={styles.cardTitle}>{item.name}</Text>
            <Image style={styles.cardImage} source={{ uri: (item.image) }} />
        </View>

    const onCarouselItemChange = (index) => {
        let location = allLocation[index];
        MapViewRef.current.animateToRegion({
            latitude: location.geoLocation.coordinates[1],
            longitude: location.geoLocation.coordinates[0],
            latitudeDelta: 0.09,
            longitudeDelta: 0.035
        })
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                <View style={{ flex: 3, }}  >
                    <View style={styles.container}>
                        <MapView
                            ref={MapViewRef}
                            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                            style={styles.map}
                            region={{
                                latitude: thisLocation.latitude,
                                longitude: thisLocation.longitude,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }}
                        >{allLocation.map((item, index) => {
                            return (
                                <Marker draggable
                                    coordinate={{ latitude: item.geoLocation.coordinates[1], longitude: item.geoLocation.coordinates[0] }}
                                    onPress={() => props.navigation.navigate('StoreDetail', { dataStore: item, indexStore: index })}
                                    title={item.name}
                                    centerOffset={{ x: -18, y: -60 }}
                                    anchor={{ x: 0.69, y: 1 }}
                                >
                                    <Image source={{ uri: (item.image) }} style={{ height: 35, width: 35, borderRadius: 25, }} />
                                    <Callout>
                                        <Text>{item.name}</Text>
                                    </Callout>
                                </Marker>
                            )
                        }
                        )
                            }
                        </MapView>
                        <Carousel
                            data={allLocation}
                            containerCustomStyle={styles.carousel}
                            renderItem={renderCarouselItem}
                            sliderWidth={Dimensions.get('window').width}
                            itemWidth={300}
                            removeClippedSubviews={false}
                            onSnapToItem={(index) => onCarouselItemChange(index)}

                        />
                    </View>
                </View>
            </SafeAreaView>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    carousel: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 48
    },
    cardContainer: {
        backgroundColor: Colors.yellow700,
        height: 200,
        width: 300,
        padding: 24,
        borderRadius: 24
    },
    cardImage: {
        height: 120,
        width: 300,
        bottom: 0,
        position: 'absolute',
        borderBottomLeftRadius: 24,
        borderBottomRightRadius: 24
    },
    cardTitle: {
        color: 'white',
        fontSize: 22,
        alignSelf: 'center'
    }

});
export default MapAll


