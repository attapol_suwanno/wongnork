import React, { useState, useEffect, useRef } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet,TouchableOpacity } from 'react-native';
import { Button, TextInput, Avatar, Headline,IconButton, Colors } from 'react-native-paper';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import axios from 'axios'

const EditStore = (props) => {
    const [idOneStore, setIdOneStore] = useState(null)
    const [imagePro, setImagePro] = useState('')
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const textInputNameStore = useRef()
    const textInputDescription = useRef()
    const { latitudeReducer, logtitudeReducer } = props

    useEffect(() => {
        setIdOneStore(props.route.params.idStore)
        setName(props.route.params.dataStore.name)
        setDescription(props.route.params.dataStore.description)
        setImagePro(props.route.params.dataStore.image)
    }, [])

    const addStoreData = () => {
        axios.put(`http://wongnork.unomy.pw/restaurant/${idOneStore}/`, { name: name, description: description, latitude: latitudeReducer, longitude: logtitudeReducer }, {
            headers: {
                Authorization: `Bearer ${props.tokenUser}`
            }
        })
            .then(response => {
                alert('Edit Successful')
                props.navigation.goBack()
            })
            .catch(error => {
                console.log('errror', error)
            })
    }

    const changeRestaurentImage = () => {
        ImagePicker.showImagePicker({}, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                const formData = new FormData()
                formData.append('image', {
                    uri: image.uri,
                    name: image.fileName,
                    type: image.type,
                })
                axios.put(`http://wongnork.unomy.pw/restaurant/${idOneStore}/image`, formData, {
                    headers: {
                        Authorization: `Bearer ${props.tokenUser}`
                    }
                })
                    .then(response => {
                        console.log('res', response.data)
                    })
                    .catch(error => {
                        console.log('eror', JSON.stringify(error))
                    })
            }
        });
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}>
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', borderRadius: 40, backgroundColor: Colors.yellow700, marginLeft: '10%', marginRight: '10%', marginTop: '10%' }} >
                        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <Headline>{name}</Headline>
                        </View>
                        <View style={{ flexDirection: 'row', }} >
                            <View style={{ flex: 1, flexDirection: 'column', padding: '3%', justifyContent: 'center', alignItems: 'center' }} >
                                <TouchableOpacity onPress={changeRestaurentImage}  >
                                    <Avatar.Image size={80}
                                        source={{
                                            uri: (imagePro)
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', padding: '3%', justifyContent: 'center', alignItems: 'center' }} >
                                <TouchableOpacity onPress={() => props.navigation.navigate('AddMap')}  >
                                    <IconButton
                                        icon="map-plus"
                                        size={80}
                                        onPress={() => props.navigation.navigate('AddMap')}
                                        style={{ backgroundColor: '' }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderRadius: 40, backgroundColor: "#ffa726", margin: '10%' }} >
                        <TextInput
                            ref={textInputNameStore}
                            mode='outlined'
                            style={{ width: "70%" }}
                            label='Namestore'
                            value={name}
                            onChangeText={text => setName(text)}
                            returnKeyType="next"
                            textContentType="name"
                            onSubmitEditing={() => textInputDescription.current.focus()}
                        />
                        <TextInput
                            ref={textInputDescription}
                            mode='outlined'
                            style={{ width: "70%" }}
                            label='Description'
                            value={description}
                            onChangeText={text => setDescription(text)}
                            multiline
                            onSubmitEditing={addStoreData}
                        />
                        <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={addStoreData} >
                            <Text> Edit </Text>
                        </Button>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45,
        margin: '2%'
    },
    stretch: {
        width: '60%',
        height: '60%',
        borderRadius: 40,
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}

export default connect(mapStateToProps, null)(EditStore)





