import React, { useState, useEffect, useRef } from 'react';
import { View, Text, ImageBackground, StatusBar, SafeAreaView, ScrollView, StyleSheet } from 'react-native';
import { Button, TextInput,  Avatar, IconButton, Colors, Title,Paragraph } from 'react-native-paper';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import axios from 'axios'

const AddStore = (props) => {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const textInputNameStore = useRef()
    const textInputDescription = useRef()
    const { latitudeReducer, logtitudeReducer } = props


    const addStoreData = () => {
        axios.post('http://wongnork.unomy.pw/restaurant', { name: name, description: description, latitude: latitudeReducer, longitude: logtitudeReducer }, {
            headers: {
                Authorization: `Bearer ${props.tokenUser}`
            }
        })
            .then(response => {
                alert('Please Upload Image')
                changeRestaurentImage(response.data._id)
            })
            .catch(error => {
                console.log('errror', error.response)
            })
    }

    const changeRestaurentImage = (id) => {
        ImagePicker.showImagePicker({}, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                const formData = new FormData()
                formData.append('image', {
                    uri: image.uri,
                    name: image.fileName,
                    type: image.type,
                })
                axios.put(`http://wongnork.unomy.pw/restaurant/${id}/image`, formData, {
                    headers: {
                        Authorization: `Bearer ${props.tokenUser}`
                    }
                })
                    .then(response => {
                        console.log('res', response.data)
                        alert('Add Store Successful ')
                    })
                    .catch(error => {
                        console.log('eror', JSON.stringify(error))
                    })
            }
        });
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <ImageBackground style={{ flex: 1, backgroundColor: Colors.yellow700 }}>
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 1, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', borderRadius: 40, backgroundColor: 'white', margin: '10%' }} >
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%',marginTop:'3%' }}  >
                        <Title>Step 1 :Input value</Title>
                    </View>
                        <TextInput
                            ref={textInputNameStore}
                            mode='outlined'
                            style={{ width: "70%" }}
                            label='Namestore'
                            value={name}
                            onChangeText={text => setName(text)}
                            returnKeyType="next"
                            textContentType="name"
                            onSubmitEditing={() => textInputDescription.current.focus()}

                        />
                        <TextInput
                            ref={textInputDescription}
                            mode='outlined'
                            style={{ width: "70%" }}
                            label='Description'
                            value={description}
                            onChangeText={text => setDescription(text)}
                            multiline
                            onSubmitEditing={addStoreData}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', borderRadius: 40, backgroundColor: Colors.white, marginLeft: '10%', marginRight: '10%', margin: '5%' }} >
                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%' }}  >
                        <Title>Step 2 : Select location</Title>
                    </View>
                        <View style={{ flex: 1, flexDirection: 'row', padding: '3%', justifyContent: 'center', alignItems: 'center' }} >
                            <IconButton
                                icon="map-plus"
                                size={60}
                                onPress={() => props.navigation.navigate('AddMap')}
                                style={{ backgroundColor: '' }}
                            />
                        </View>
                        <View style={{ flex: 0, flexDirection: 'row', padding: '3%', justifyContent: 'center', alignItems: 'center' }} >
                        <Paragraph>Click add to import Image Store</Paragraph>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', padding: '1%', justifyContent: 'center', alignItems: 'center' }} >
                            <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={addStoreData} >
                                <Text> Add Store</Text>
                            </Button>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
});

const mapStateToProps = state => {
    return {
        latitudeReducer: state.locationNow.latitude,
        logtitudeReducer: state.locationNow.longitude,
        tokenUser: state.tokenLogin.token,
    }
}

export default connect(mapStateToProps, null)(AddStore)





