import React, { useEffect, useState } from 'react';
import { View, Text,  StatusBar, SafeAreaView, ScrollView, StyleSheet, Image, } from 'react-native';
import { Button,Title} from 'react-native-paper';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';

const AddImage = (props) => {
    const [dataUser, setDatauser] = useState([])
    const [imageUpadte, setImageUpadte] = useState([])

    useEffect(() => {
        setDatauser(props.route.params.sentDataProfile)
    }, [])


    const changeImage = () => {
        ImagePicker.showImagePicker({}, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                const formData = new FormData()
                formData.append('image', {
                    uri: image.uri,
                    name: image.fileName,
                    type: image.type,
                })
                axios.post(`http://wongnork.unomy.pw/user/profile/image`, formData, {
                    headers: {
                        Authorization: `Bearer ${props.tokenUser}`
                    }
                })
                    .then(response => {
                        console.log('res', response.data)
                        setImageUpadte(response.data.url)
                            (props.navigation.navigate('Home'))
                    })
                    .catch(error => {
                        console.log('eror', JSON.stringify(error))
                    })
            }
        });
    }

    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <StatusBar translucent={true} backgroundColor={'transparent'} barStyle="dark-content" />
                <SafeAreaView style={{ flex: 2, }} forceInset={{ top: 'always' }}  >
                    <View style={{ flex: 1, flexDirection: 'column', borderRadius: 40, backgroundColor: "#ffa726", alignItems: 'center' }} >
                        <Image style={styles.stretch}
                            // source={{ uri: (profileImage) }}
                            source={{ uri: 'https://i.pinimg.com/originals/74/44/03/744403ceee06ccfe326f279a83a61934.jpg' }}
                        >
                        </Image>
                        <Title>{dataUser.username}</Title>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginLeft: '5%', marginRight: '5%', }}  >
                        <Button style={styles.button} color="#18ffff" icon="logout-variant" mode="contained" onPress={changeImage} >
                            <Text> AddPicture</Text>
                        </Button>
                    </View>
                </SafeAreaView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    stretch: {
        width: '50%',
        height: '60%',
        borderRadius: 100,
        marginTop: '15%'
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
    }
}

export default connect(mapStateToProps, null)(AddImage)
