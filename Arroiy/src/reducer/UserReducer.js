import { ADD_USERDATA } from '../action/UserAction'
import { ADD_DATAPROFILE } from '../action/UserAction'
const innitalstate = {
  user: []

}

export const addUserReducer = (state = innitalstate, action) => {
  switch (action.type) {

    case ADD_USERDATA:
      return {
        user: action.addUserData
      }
    case ADD_DATAPROFILE:
      return {
        user: action.payLoad
      }
    default:
      return state
  }
};
