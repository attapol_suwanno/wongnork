const innitalstate = {

  restaurentData: {}
};

export const restaurantReducer = (state = innitalstate, action) => {
  switch (action.type) {
    case 'RESTAURANT_DATA_SUCCESS':
      return {
        ...state,
        restaurentData: action.payLoad,
      }
    default:
      return state
  }
};
