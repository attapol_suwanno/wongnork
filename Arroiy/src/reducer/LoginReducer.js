import { ADD_TOKEN } from '../action/LoginAction'
import { REMOVE_TOKEN } from '../action/LoginAction'
const innitalstate = {
  token: null
}

export const loginReducer = (state = innitalstate, action) => {
  switch (action.type) {
    case ADD_TOKEN:
      return {
        token: action.addToken
      }
    case REMOVE_TOKEN:
      return {
        token: null
      }
    default:
      return state
  }
};
