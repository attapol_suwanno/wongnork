import { combineReducers } from 'redux';
import { restaurantReducer } from './RestaurentReducer';
import { loginReducer } from './LoginReducer'
import { locationReducer } from './LocationReducer'
import { addUserReducer } from './UserReducer'
import { fisrtUsedReducer } from './FirstUseReducer'

export const rootReducer = combineReducers({
    dataAllRestaurent: restaurantReducer,
    tokenLogin: loginReducer,
    locationNow: locationReducer,
    dataProfile: addUserReducer,
    locationFirst: fisrtUsedReducer
})