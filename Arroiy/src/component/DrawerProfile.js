import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Animated, } from 'react-native';
import { DrawerContentScrollView, } from '@react-navigation/drawer';
import { Button, Title, Avatar } from 'react-native-paper';
import { connect } from 'react-redux';
import { logOut } from '../action/LoginAction';
import axios from 'axios';

const DrawerProfile = (props) => {
    const { DataOneProfile } = props;

    const renderButton = () => {
        if (props.tokenUser !== null) {
            return <View style={{ flex: 0, flexDirection: 'column', justifyContent: 'center', marginTop: '4%' }}  >
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', }}  >
                    <TouchableOpacity color="#18ffff" icon="logout-variant" mode="contained"  >
                        <Avatar.Image size={70}
                            source={{ uri: (DataOneProfile.profileImage) }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', marginTop: '4%' }}  >
                    <Title>{DataOneProfile.username}</Title>
                </View>
            </View>
        }
    }


    const logout = () => {
        props.logOut()
        axios.post('http://wongnork.unomy.pw/auth/logout', {
            headers: {
                Authorization: `Bearer ${props.tokenUser}`
            }
        })
            .then(response => {
                props.navigation.navigate('FirstUseApp')
          
            })
            .catch(error => {
                console.log('error', JSON.stringify(error))
            })
       
    }

    const loginLogoutButton = () => {
        if (props.tokenUser !== null) {
            return <Button style={styles.button} color="#e53935" icon="logout-variant" mode="contained" onPress={() => logout()} >
                <Text> logout</Text>
            </Button>
        }
        else {
            return <Button style={styles.buttonLogout} color="#18ffff" icon="logout-variant" mode="contained" onPress={() => props.navigation.navigate('Login')} >
                <Text> login</Text>
            </Button>

        }
    }

    return (
        <DrawerContentScrollView  {...props} >
            {renderButton()}
            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', marginTop: '5%' }}  >
                {loginLogoutButton()}
            </View>
            <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', marginTop: '5%' }}  >
                <Button style={styles.buttonLogout} color="#18ffff" icon="logout-variant" mode="contained" onPress={() => props.navigation.navigate('MapSearch')} >
                    <Text> Add location</Text>
                </Button>
            </View>
        </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({
    button: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    },
    buttonLogout: {
        zIndex: 0,
        borderRadius: 25,
        width: "60%",
        height: "100%",
        justifyContent: 'center',
        elevation: 25,
        height: 45
    }
});

const mapStateToProps = state => {
    return {
        tokenUser: state.tokenLogin.token,
        DataOneProfile: state.dataProfile.user
    }
}

const mapDispatchToProps = {
    logOut,
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerProfile)