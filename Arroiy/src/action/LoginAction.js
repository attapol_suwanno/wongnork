export const ADD_TOKEN = 'ADD_TOKEN'
export const  REMOVE_TOKEN= ' REMOVE_TOKEN'
export const addTokenId = (token) => {
    return {
        type: ADD_TOKEN,
        addToken: token
    }
}
export const logOut = () => {
    return {
        type: REMOVE_TOKEN,
    }
}