export const ADD_USERDATA = 'ADD_USERDATA'
export const ADD_DATAPROFILE = 'ADD_DATAPROFILE'

export const addUserData = (token) => {
    return {
        type: ADD_USERDATA,
        addUserData: token
    }
}

export const addUserProfile = (item) => {
    return {
        type: 'ADD_DATAPROFILE',
          payLoad:item
    }
}