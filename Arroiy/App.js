/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { View, Text } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import 'react-native-gesture-handler';
import thunk from 'redux-thunk';
import FirstUseApp from './src/screen/FirstUseApp';
import Router from './src/router/Router';
import { rootReducer } from './src/reducer/index'
import AsyncStorage from '@react-native-community/async-storage';
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore, persistReducer } from 'redux-persist'
import mainRouter from '../Arroiy/src/router/mainRouter'


const persistConfig = {
  key: 'root',
  storage:AsyncStorage
}
const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(persistedReducer);
const persistor = persistStore(store)

const App = () => {
  return (
    <Provider store={store} >
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider>
          <Router />
        </PaperProvider>
      </PersistGate>
    </Provider>

  );
};

export default App;
