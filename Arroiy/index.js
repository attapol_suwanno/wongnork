/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
import mainRouter from '../Arroiy/src/router/mainRouter'
AppRegistry.registerComponent(appName, () => App);
